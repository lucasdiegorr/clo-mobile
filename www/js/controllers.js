angular.module('starter.controllers', ['ionic','ngCordova'])

.controller('DashCtrl', function($scope) {})

.controller('CameraCtrl', function($state, $sanitize, $scope,$cordovaBarcodeScanner,$ionicPlatform) {

  $scope.scan = function(){
    $ionicPlatform.ready(function() {
      $cordovaBarcodeScanner.scan().then(function(barcodeData) {
        var data = JSON.parse(barcodeData.text);
        $state.go('tab.chats', { nickname: data.nickname, hostname: data.hostname, project_owner: data.project_owner, project_id: data.project_id })
      }, function(error) {
          alert(JSON.stringify(error));
      });
    });
  }
  $scope.$on("$ionicView.loaded", $scope.scan());
})

.controller('ViewCtrl', function($scope, $cordovaInAppBrowser){
   var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'no'
    };

    $scope.$on("$ionicView.enter", function(event, data){
      $cordovaInAppBrowser.open('http://google.com', '_self', options)
      .then(function(event) {})
      .catch(function(event) {});
    });


})

.controller('ChatsCtrl',function($stateParams, SocketFactory, $sanitize, $ionicScrollDelegate, $timeout) {
    
    var self = this;
    
    //Add colors
    var COLORS = [
      '#e21400', '#91580f', '#f8a700', '#f78b00',
      '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
      '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
    ];

   //initializing messages array
    self.messages=[]

    socket = SocketFactory.connect($stateParams.hostname);
    
    socket.on('connect',function(){
      
      self.connected = true
     
      //Add user
      socket.emit('add user', $stateParams.nickname, $stateParams.project_owner, $stateParams.project_id, function(data){
        if (data) {} 
        else {}
      });
      //var roomname = String($stateParams.project_owner + ' ' + $stateParams.project_id).toLowerCase();
      //socket.to(roomname).emit('mobile connect', {username: $stateParams.nickname});

      // Whenever the server emits 'new message', update the chat body
      socket.on('update chat', function (data) {
        if(data.value && data.user){
          if ($stateParams.nickname != data.user) {
            addMessageToList(data.user,true,data.value)
          }else{
            addMessageToList("me",true,data.value)
          }
        }
      });

    });

    //function called when user hits the send button
    self.sendMessage = function(){
      socket.emit('send msg', self.message)
      self.message = ""
    }

    // Display message by adding it to the message list
    function addMessageToList(username,style_type,message){
      username = $sanitize(username)
      var color = style_type ? getUsernameColor(username) : null
      self.messages.push({content:$sanitize(message),style:style_type,username:username,color:color})
      $timeout($ionicScrollDelegate.scrollBottom(),400);
    }

    //Generate color for the same user.
    function getUsernameColor (username) {
      // Compute hash code
      var hash = 7;
      for (var i = 0; i < username.length; i++) {
         hash = username.charCodeAt(i) + (hash << 5) - hash;
      }
      // Calculate color
      var index = Math.abs(hash % COLORS.length);
      return COLORS[index];
    }

})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});

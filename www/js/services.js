angular.module('starter.services', [])

.factory('SocketFactory',function(){
  return {
    connect: function (ipserver) {
      return io.connect(ipserver + '/r');
    }
  };
});